package learnspring.service;

import learnspring.model.MyUser;
import learnspring.model.MyUserDetails;
import org.springframework.security.core.userdetails.ReactiveUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.List;

@Component
public class MyReactiveUserDetailsService implements ReactiveUserDetailsService {

    private final PasswordEncoder passwordEncoder;
    private final List<MyUser> myUsers;

    public MyReactiveUserDetailsService(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
        this.myUsers = Arrays.asList(
                new MyUser("gene", passwordEncoder.encode("password"), "I like food"));
    }

    @Override
    public Mono<UserDetails> findByUsername(String username) {
        MyUserDetails myUserDetails = myUsers.stream()
                .filter(user -> user.email.equals(username))
                .findFirst()
                .map(user -> new MyUserDetails(user))
                .orElseThrow(() -> new RuntimeException("User not found"));

        return Mono.just(myUserDetails);
    }
}
