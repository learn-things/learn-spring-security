package learnspring.model;

public class MyUser {

    public String email;
    public String password;
    public String myProperty;

    public MyUser(String email, String password, String myProperty) {
        this.email = email;
        this.password = password;
        this.myProperty = myProperty;
    }
}

