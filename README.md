# Reactive Spring Security with WebFlux
- Spring Boot version: `2.2.5`

## 1. Minimal configuration setup
### Setup gradle
Add the `spring-boot-starter-security` dependency in `build.gradle`
```
dependencies {
    // ... 
    implementation 'org.springframework.boot:spring-boot-starter-security'
}
```

### Create a controller 
```java
@RestController
public class MyController {

    @GetMapping("/")
    public Mono getHome() {
        return Mono.just("Hello World");
    }
}
```

### Create Spring Security Configuration 
Create a `@Configuration` for Spring Security.
```java
@Configuration
@EnableWebFluxSecurity
public class SecurityConfig {

    /**
     * Spring Security uses ReactiveUserDetailsService to find users for authentication and authorization.
     * MapReactiveUserDetailsService is an implementation of ReactiveUserDetailsService
     */
    @Bean
    public MapReactiveUserDetailsService userDetailsService() {
        UserDetails user = User.withDefaultPasswordEncoder()
                .username("user")
                .password("user")
                .roles("USER")
                .build();
        return new MapReactiveUserDetailsService(user);
    }

    /**
     * SecurityWebFilterChain describes the authorization
     */
    @Bean
    public SecurityWebFilterChain springSecurityFilterChain(ServerHttpSecurity http) {
        http
                .authorizeExchange(exchanges -> exchanges
                        .anyExchange().authenticated()
                )
                .httpBasic(withDefaults())
                .formLogin(withDefaults());
        return http.build();
    }

    /**
     * The DelegatingPasswordEncoder is the default PasswordEncoder
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }

}
```

### Try it out
Run the application and try the following
1. Navigate to `localhost:8080`
   - You should be redirected to `localhost:8080/login` and a sign in form should be rendered
   - ![default login](./img/default-login.png)
2. Login using `user` for both username and password
   - You should be redirected to `localhost:8080/` and see "Hello World"
3. Navigate to `localhost:/8080/logout` 
   - You should see a logout button
   - ![default logout](./img/default-logout.png)
4. Click the logout button
   - You should be redirected to `locahost:8080/login?logout`
   - You should see the login form again

## 2. Using your User model for authentication and authorization
### Create your user model
```java
public class MyUser {

    public String email;
    public String password;
    public String permission;

    public MyUser(String email, String password) {
        this.email = email;
        this.password = password;
    }
}
```

### Create your `ReactiveUserDetailsService`
```java
@Component
public class MyReactiveUserDetailsService implements ReactiveUserDetailsService {

    @Override
    public Mono<UserDetails> findByUsername(String username) {
        return null;
    }
}
```
Spring Security uses `UserDetails` interface for authentication and authorization. Creating another class that extends to your user model and implements `UserDetails` allows Spring Security to use your user model for authentication and authorization.

### Create your `UserDetails`
```java
public class MyUserDetails extends MyUser implements UserDetails {

    public MyUserDetails(MyUser user) {
        super(user.email, user.password);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return AuthorityUtils.createAuthorityList("USER");
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() { return true; }

    @Override
    public boolean isAccountNonLocked() { return true; }

    @Override
    public boolean isCredentialsNonExpired() { return true; }

    @Override
    public boolean isEnabled() { return true; }
}
```
You can implements the methods according to your user model.

### Update your `ReactiveUserDetailsService`
```java
@Component
public class MyReactiveUserDetailsService implements ReactiveUserDetailsService {

    private final PasswordEncoder passwordEncoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
    private final List<MyUser> myUsers = Arrays.asList(
            new MyUser("user", passwordEncoder.encode("user")));

    @Override
    public Mono<UserDetails> findByUsername(String username) {
        MyUserDetails myUserDetails = myUsers.stream()
                .filter(user -> user.email.equals(username))
                .findFirst()
                .map(user -> new MyUserDetails(user))
                .orElseThrow(() -> new RuntimeException("User not found"));

        return Mono.just(myUserDetails);
    }
}
```
Add a way to get your users and return the `UserDetails` instance. I am using a user list field, but you can replace this with a repository that connects to a database. Also I have used the [DelegatingPasswordEncoder](/howto/spring/spring-security-delegating-password-encoder) to encode the password.

### Remove the MapReactiveUserDetailsService
Since we now have a custom `ReactiveUserDetailsService`, we need to remove the `MapReactiveUserDetailsService` so that Spring Security does not get confused which `ReactiveUserDetailsService` to use. So we are left with the following.
```java
@Configuration
@EnableWebFluxSecurity
public class SecurityConfig {

    /**
     * SecurityWebFilterChain describes the authentication and authorization
     */
    @Bean
    public SecurityWebFilterChain springSecurityFilterChain(ServerHttpSecurity http) {
        http
                .authorizeExchange(exchanges -> exchanges
                        .anyExchange().authenticated()
                )
                .httpBasic(withDefaults())
                .formLogin(withDefaults());
        return http.build();
    }

    /**
     * The DelegatingPasswordEncoder is the default PasswordEncoder
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }

}
```

### Try it out
Run the application and try the following
1. Navigate to `localhost:8080`
   - You should be redirected to `localhost:8080/login` and a sign in form should be rendered
   - ![default login](./img/default-login.png)
2. Login using `user` for both username and password
   - You should be redirected to `localhost:8080/` and see "Hello World"
3. Navigate to `localhost:/8080/logout` 
   - You should see a logout button
   - ![default logout](./img/default-logout.png)
4. Click the logout button
   - You should be redirected to `locahost:8080/login?logout`
   - You should see the login form again

# Reference
- [Spring Security Reference](https://docs.spring.io/spring-security/site/docs/current/reference/html5/)
